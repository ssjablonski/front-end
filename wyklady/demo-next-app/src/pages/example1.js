const fruits = ["banan", "apple", "grape"]

function SayHello(props) {
    return (
        <div>
            <div>
            Hello {props.firstname}
            </div>
            <div>
                {props.lastname}
            </div>
            <ul>
                {fruits.map(fruit => (<li>{fruit}</li>))}
            </ul>
        </div>
    )
}



function Ex() {
    return (
        <div>
            <SayHello firstname="zdzirki" lastname="z UG"></SayHello>
        </div>
    )
}

export default Ex()