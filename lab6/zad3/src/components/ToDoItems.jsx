import {useState, React} from 'react';
import ToDoForm from './ToDoForm';

function ToDoItems() {
    const [toDos, setToDos] = useState([]);

    function handleAdd(text, data) {
        const newToDos = [...toDos]
        newToDos.push({id: toDos.length, text: text, data: data})
        setToDos(newToDos);
    }

    return (
        <div>
            <h1>MY TO DOS:</h1>
            <ol>
                {toDos.map((todo) => (
                    <li key={todo.id}>Treść: {todo.text}   Data: {todo.data}</li>
                ))}
            </ol>
            <ToDoForm onAdd={handleAdd}/>
        </div>
    );
}

export default ToDoItems;