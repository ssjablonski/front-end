import React from 'react';

function FormToDoItem(props) {
    return (
        <input 
            type="text"
            name={props.name}
            value={props.value}
            required
            onChange={props.onChange}
            placeholder='Treść'
        />
    )
}

export default FormToDoItem;