import {useState, useEffect, React} from 'react';
import FormToDoItem from './FormToDoItem';
import FormToDoData from './FormToDoData';
import FormToDoMessages from './FormToDoMessages';

function ToDoForm(props) {
    const [text, setText] = useState("");
    const [data, setData] = useState("");
    const [message, setMessage] = useState("");


    function handleChange(event) {
        setMessage("");
        switch(event.target.name) {
            case 'text':
                setText(event.target.value);
                break;
            case 'data':
                setData(event.target.value);
                break;
            default:
                break;
        }
    }

    function handleSubmit(event) {
        event.preventDefault();
        props.onAdd(text,data);
        setText("");
        setData("");
        setMessage("Dodano zadanie!");
    }

    return (
        <form onSubmit={(event) => handleSubmit(event, text)}>
            <FormToDoItem name="text" value={text} onChange={handleChange}/>
            <FormToDoData name="data" data={data} onChange={handleChange} />
            <button type="submit">Add</button>
            <FormToDoMessages name="message" value={message}/>
        </form>
    );
}

export default ToDoForm;