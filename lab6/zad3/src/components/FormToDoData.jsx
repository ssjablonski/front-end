import {useState, React} from 'react';

function FormToDoData(props) {
    const today = new Date().toISOString().split('T')[0];

    return (
        <input 
            type="date" 
            name={props.name} 
            value={props.data}
            min={today} 
            required 
            onChange={props.onChange}
        />
    );
}

export default FormToDoData;