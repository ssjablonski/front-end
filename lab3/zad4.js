
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let firstLine = null;
let secondLine = null;
let isSecondLine = false;

rl.on('line', (line) => {
  if (!isSecondLine) {
    firstLine = line;
    isSecondLine = true;
  } else {
    secondLine = line;
    
    planety = {
        "Ziemia": 31557600,
        "Merkury": 0.2408467 * 31557600,
        "Wenus": 0.61519726 * 31557600,
        "Mars": 1.8808158 * 31557600,
        "Jowisz": 11.862615 * 31557600,
        "Saturn": 29.447498 * 31557600,
        "Uran": 84.016846 * 31557600,
        "Neptun": 164.79132 * 31557600
    }
    const a = firstLine/planety[secondLine]
    console.log(Math.round(a*100)/100)
  }
});






