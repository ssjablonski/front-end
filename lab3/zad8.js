const num = [2 ,2.2 ,6 ,8.6 ,21 ,37.7]

function answer(tablica) {
    return tablica.reduce((acc, val) => {
        if (val**2 %1 == 0) {
            const res = val**2
            acc.push(res.toFixed(2))
        }
        return acc
    }, [])
}

console.log(answer(num))

