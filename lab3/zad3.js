const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.on('line', (input) => {
    const digits = input.split('').map(Number);
    let wynik = 0

    digits.forEach((liczba) => wynik += Math.pow(liczba, digits.length))
    if (wynik == input) {
        console.log(true)
    } else {
        console.log(false)
    }
});







