async function asyncDivide(dividend, divisor) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (divisor === 0) {
        reject('Nie można dzielić przez zero.');
      } else {
        resolve(dividend / divisor);
      }
    }, 1000);
  });
}