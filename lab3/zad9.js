
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.on('line', (input) => {
    const words = input.split(' ');
     console.log(answer(words))
});


function answer(words) {
    const histogram = words.reduce((acc, word) => {
      if (word in acc) {
        acc[word]++;
      } else {
        acc[word] = 1;
      }
      return acc;
    }, {});

    return histogram
}