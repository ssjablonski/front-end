function answer(list, start, stop, search) {
  if (start > stop) {
    return "Brak elementu w ciągu!!!";
  }

  const q = Math.floor((start + stop) / 2);

  if (list[q] === search) {
    return q;
  } else if (list[q] > search) {
    return answer(list, start, q - 1, search);
  } else {
    return answer(list, q + 1, stop, search);
  }
}
