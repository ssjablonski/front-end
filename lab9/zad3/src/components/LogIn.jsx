import React, {useState} from 'react'
import {useFormik} from 'formik'
import * as Yup from 'yup'
// import './LogIn.scss';

function LogIn() {

    const [pasy, setPasy] = useState([{
        name: 'admin',
        email: 's@onet.pl',
        password: 'admin123',
        obywatelstwo: 'Polska',
    }]);

    function handleReset() {
        formik.resetForm();
    }

    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            radiobutton1: false,
            radiobutton2: false,
            obywatelstwo:'',
            checkbox: false,
    },
    validationSchema: Yup.object({
        name: Yup.string().required('Required'),
        email: Yup.string().email('Invalid email address').required('Required'),
        password: Yup.string().min(8, 'Password musi byc dluzszy niz 8 znakow').required('Required'),
        radiobutton1: Yup.bool().required('Required'),
        radiobutton2: Yup.bool().required('Required'),
        obywatelstwo: Yup.string().required('Required'),
        checkbox: Yup.bool().required('Required')
    }),
    onSubmit: values => {
        
        const konto = {
            name: values.name,
            email: values.email,
            password: values.password,
            radiobutton1: values.radiobutton1,
            radiobutton2: values.radiobutton2,
            obywatelstwo: values.obywatelstwo,
            checkbox: values.checkbox
        }
        setPasy([...pasy, konto])
    },
    });


    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                <input type="text" id='name' name='name' placeholder='name' value={formik.values.name} onChange={formik.handleChange}
                required onBlur={formik.handleBlur}/>
                {formik.touched.name && formik.errors.name ? <div>{formik.errors.name}</div> : null}
                <input type="text" id='email' name='email' placeholder='email' value={formik.values.email} onChange={formik.handleChange}
                required onBlur={formik.handleBlur}/>
                {formik.touched.email && formik.errors.email ? <div>{formik.errors.email}</div> : null}
                <input type="text" id='password' name='password' placeholder='password' value={formik.values.password} onChange={formik.handleChange}
                required onBlur={formik.handleBlur}/>
                {formik.touched.password && formik.errors.password ? <div>{formik.errors.password}</div> : null}
                <input type="radio" id='radiobutton1' name='radiobutton' value={formik.values.radiobutton1} onChange={formik.handleChange}
                required onBlur={formik.handleBlur}/>
                <input type="radio" id='radiobutton2' name='radiobutton' value={formik.values.radiobutton2} onChange={formik.handleChange}
                required onBlur={formik.handleBlur}/>
                <input type="text" id='obywatelstwo' name='obywatelstwo' value={formik.values.obywatelstwo} onChange={formik.handleChange}
                required onBlur={formik.handleBlur} />
                {formik.touched.obywatelstwo && formik.errors.obywatelstwo ? <div>{formik.errors.obywatelstwo}</div> : null}
                <input type="checkbox" id='checkbox' name='checkbox' value={formik.values.checkbox} onChange={formik.handleChange}
                required onBlur={formik.handleBlur}/>
                <button type="submit">Log In</button>
                <button onClick={handleReset}>Reset</button>
            </form>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Obywatelstwo</th>
                    </tr>
                </thead>
                <tbody>
                    {pasy.map((konto, index) => (
                        <tr key={index}>
                            <td>{konto.name}</td>
                            <td>{konto.email}</td>
                            <td>{konto.password}</td>
                            <td>{konto.obywatelstwo}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default LogIn