import React from 'react'
import {useFormik} from 'formik'
import * as Yup from 'yup'

function LogIn() {
    const passy = {
        username: 'admin',
        password: 'admin123'
    }
    const validationSchema = Yup.object({
        username: Yup.string().required('Required').min(5, 'Username musi byc dluzszy niz 5 znakow'),
        password: Yup.string().required('Required').min(8, 'Password musi byc dluzszy niz 8 znakow')
    })

    const formik = useFormik({
        initialValues: {
            username: '',
            email: '',
        },
        validationSchema: validationSchema,
        onSubmit: values => {
            if (values.username === passy.username && values.password === passy.password) {
                alert('Zalogowano poprawnie');
            } else {
                alert('Niepoprawne dane');
            }
        },
    });
    return (
      <form onSubmit={formik.handleSubmit}>
        <input
          type="text"
          id='username'
          name='username'
          placeholder="username"
          value={formik.values.username}
          onChange={formik.handleChange}
          required
          onBlur={formik.handleBlur}
        />
        {formik.touched.username && formik.errors.username ? <div>{formik.errors.username}</div> : null}
        <input
          type="password"
          name='password'
          id='password'
          placeholder="password"
          value={formik.values.password}
          onChange={formik.handleChange}
          required
          onBlur={formik.handleBlur}
        />
        {formik.touched.password && formik.errors.password ? <div>{formik.errors.password}</div> : null}
        <button type="submit">Log In</button>
      </form>
    )
}

export default LogIn