import React, { useState } from 'react';

function LogIn() {

    const pass = {
        login: "admin",
        password: "admin"
    }
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [isLogged, setIsLogged] = useState(false);

    function handleChange(e) {
        setIsLogged(false);
        setError("");
        switch (e.target.name) {
            case "username":
                if (e.target.value.length === 0) {
                    setError("")
                } else if (e.target.value.length < 5) {
                    setError("Nazwa użytkownika oraz hasło muszą mieć conajmniej 5 znaków");
                } else {
                    setError("");
                }
                setUsername(e.target.value);
                break;
            case "password":
                if (e.target.value.length === 0) {
                    setError("")
                } else if (e.target.value.length < 5) {
                    setError("Nazwa użytkownika oraz hasło muszą mieć conajmniej 5 znaków");
                } else {
                    setError("");
                }
                setPassword(e.target.value);
                break;
            default:
                break;
        }
    }

  const handleSubmit = (e) => {
    e.preventDefault();
    if (pass.login === username && pass.password === password) {
        console.log(username, password);
        setIsLogged(true);
        setUsername('');
        setPassword('');
    } else {
        setIsLogged(false);
        setUsername('');
        setPassword('');
    }}

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name='username'
          placeholder="username"
          value={username}
          onChange={handleChange}
          required
        />
        <input
          type="password"
          name='password'
          placeholder="password"
          value={password}
          onChange={handleChange}
          required
        />
        <button type="submit">Log In</button>
      </form>
        {error ? <p>{error}</p> : null}
        {isLogged && <p>Logged in!</p>}
    </div>
  );
  }

export default LogIn;