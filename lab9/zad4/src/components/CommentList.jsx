import React, { useEffect, useState } from 'react';
import axios from 'axios';

function CommentList(props) {
    const [comments, setComments] = useState([]);

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/comments')
            .then(response => {
                setComments(response.data);
            })
    }, [props.render]);

    return (
        <ul>
            {comments.map(comment => (
                <li key={comment.id}>
                    {comment.email} - {comment.name}
                </li>
            ))}
        </ul>
    );
}

export default CommentList;