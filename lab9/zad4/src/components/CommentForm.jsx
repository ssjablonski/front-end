import React from 'react'
import {useFormik} from 'formik'
import * as Yup from 'yup'
import CommentList from './CommentList'
import axios from 'axios'

function CommentForm() {

    const [render, setRender] = React.useState(false); // <-- added state
    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            body: '',
        },
        validationSchema: Yup.object({
            name: Yup.string().min(2, 'Name musi być dłuższy niż jeden znak').max(20, 'Name może być maksymalnie 20 znaków').required('Required'),
            email: Yup.string().email('Invalid email address').required('Required'),
            body: Yup.string().optional()
        }),
        onSubmit: async values => {
             await axios.post('https://jsonplaceholder.typicode.com/comments', 
             {name: values.name, email: values.email, body: values.body})
            .then(() => {
                formik.resetForm();
                setRender(!render);
            });
            alert("Dodano komentarz")
        },
    })

    function handleReset() {
        formik.resetForm();
    }

    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                <input type="text" id='name' name='name' placeholder='name' value={formik.values.name} onChange={formik.handleChange}
                required onBlur={formik.handleBlur}/>
                {formik.touched.name && formik.errors.name ? <div>{formik.errors.name}</div> : null}
                <input type="text" id='email' name='email' placeholder='email' value={formik.values.email} onChange={formik.handleChange}
                required onBlur={formik.handleBlur}/>
                {formik.touched.email && formik.errors.email ? <div>{formik.errors.email}</div> : null}
                <input type="text" id='body' name='body' placeholder='body' value={formik.values.body} onChange={formik.handleChange}
                onBlur={formik.handleBlur}/>
                {formik.touched.body && formik.errors.body ? <div>{formik.errors.body}</div> : null}
                <button type="submit">Submit</button>
                <button onClick={handleReset}>Reset</button>
            </form>
            <CommentList render={render}/>

        </div>
    )
}

export default CommentForm