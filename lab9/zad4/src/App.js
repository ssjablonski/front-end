import './App.css';
import CommentForm from './components/CommentForm';

function App() {
  return (
    <div>
      <CommentForm />
    </div>
  );
}

export default App;
