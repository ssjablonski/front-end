import React, {useState} from 'react'
import { LoginContext } from '../context/LoginContext'

function Register() {

    const pasy = React.useContext(LoginContext)


    const [name, setName] = useState("")
    const [surname, setSurname] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [birthDate, setBirthDate] = useState("")
    const [img, setImg] = useState("")
    const [message, setMessage] = useState("Niepoprawny format email")
    const [acceptTerms, setAcceptTerms] = useState(false);
    const [passwordStrength, setPasswordStrength] = useState("Hasło musi mieć conajmniej 8 znaków i zawierać przynajmniej jeden znak specjalny");

    //   ? tu chyba  mozna tak ze stany haslo i email zrobic na true false i na tego podstawie wyswietlac komunikaty i na poczatku poprostu false dac a nie komunikat od razu

    function handleReset(e) {
        e.preventDefault()
        setName("")
        setSurname("")
        setEmail("")
        setPassword("")
        setBirthDate("")
        setImg("")
        setAcceptTerms(false)
    }


    function handleSubmit(e) {
        e.preventDefault();

        const data = {
            name: name,
            surname: surname,
            email: email,
            password: password,
            birthDate: birthDate,
            img: img
        };
        pasy.push(data)
        setName("")
        setSurname("")
        setEmail("")
        setPassword("")
        setBirthDate("")
        setImg("")
        setAcceptTerms(false)


        console.log(pasy);
    }

    function handleChange(e) {
        switch (e.target.name) {
            case "name":
                setName(e.target.value);
                break;
            case "surname":
                setSurname(e.target.value);
                break;
            case "email":
                const patterns = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/;
                if (!patterns.test(e.target.value)) {
                    setMessage("Niepoprawny format email")
                    console.log(message.length)
                } else {
                    setMessage(null)
                    console.log(message.length)
                }
                setEmail(e.target.value);
                break;
            case "password":
                const specialCharacterPattern = /[ `!@#$%^&*()_+\-=[\]{};':"|,.<>/?~]/;
                if (e.target.value.length < 8 || !specialCharacterPattern.test(e.target.value)) {
                    setPasswordStrength("Hasło musi mieć conajmniej 8 znaków i zawierać przynajmniej jeden znak specjalny");
                } else {
                    setPasswordStrength(null);
                }
                setPassword(e.target.value);
                break;
            case "birthDate":
                setBirthDate(e.target.value);
                break;
            case "img":
                setImg(e.target.value);
                break;
            default:
                break;
        }
    }
  return (
    <div>
        <form action="" onSubmit={handleSubmit}>
            <h2>Rejestracja</h2>
            <input type="text" required value={name} name="name" onChange={handleChange} placeholder='name' />
            <br />
            <input type="text" required value={surname} name="surname" onChange={handleChange} placeholder='surname' />
            <br />
            <input type="text" value={email} name="email" onChange={handleChange} placeholder='email'/>
            <span>{message}</span>
            <br />
            <input type="password" value={password} name="password" onChange={handleChange} placeholder='password'/>
            <span>{passwordStrength}</span>
            <br />
            <label>
            Data urodzenia:
            <input type="date" value={birthDate} name="birthDate" onChange={handleChange} />
            </label>
            <br />
            <label>
            Zdjęcie profilowe:
            <input type="file" accept="image/*" value={img} name="img" onChange={handleChange} />
            </label>
            <br />
            <label>
            Akceptuję warunki:
            <input type="checkbox" checked={acceptTerms} onChange={() => setAcceptTerms(!acceptTerms)} />
            </label>
            <br />
            <button disabled={!acceptTerms || passwordStrength.length !== 0 || message.length !== 0}>
                Submit
            </button>
            <button onClick={handleReset}>Reset</button>
        </form>
    </div>
  )
}

export default Register