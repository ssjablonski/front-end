import React, {useState, useEffect} from 'react'
import { LoginContext } from '../context/LoginContext'

function LogIn() {
    const pasy = React.useContext(LoginContext)

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [error, setError] = useState("")
    const [message, setMessage] = useState("")


    function handleChange(e) {
        setMessage("")
        switch (e.target.name) {
            case "email":
                    const patterns = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/;
                    if (!patterns.test(e.target.value)) {
                        setError("Niepoprawny format email")

                    } else {
                        setError("")

                    }
                    setEmail(e.target.value);
                    break;
            case "password":
                setPassword(e.target.value);
                break;
            default:
                break;
        }
    }

    function submitHandler(e) {
        e.preventDefault()
        const email = e.target.email.value
        const password = e.target.password.value
        console.log(email, password)
        if (pasy.find((user)=> user.email === email && user.password === password)) {
            setMessage("Pomyslnie zalogowano!")
            setEmail("")
            setPassword("")
        } else {
            setMessage("Niepoprawne dane logowania!")
            setEmail("")
            setPassword("")
        }
    }

    return (
        <div>
            <h1>Zaloguj się</h1>
            <form action="" onSubmit={submitHandler}>
                <input type="text" placeholder="email" name="email" id="email" value={email} onChange={handleChange}/>
                <span>{error}</span>
                <input type="password" placeholder="password" name="password" id="password" value={password} onChange={handleChange} />
                <button disabled={error.length !== 0}>Submit</button>
            </form>
            <h2>{message}</h2>
        </div>

    )
}

export default LogIn