import React, {createContext} from 'react';

const LoginContext = createContext();

const json = [
    {
        "name": "name1",
        "surname": 'surname',
        "email": "email",
        "password": "password",
        "birthDate": "birthDate",
        "img": "img"
    },
    {
        "name": "name2",
        "surname": 'surname',
        "email": "email",
        "password": "password",
        "birthDate": "birthDate",
        "img": "img"
    },
    {
        "name": "name3",
        "surname": 'surname',
        "email": "email",
        "password": "password",
        "birthDate": "birthDate",
        "img": "img"
    }
]


function LoginProvider({children}) {
    return (
    <LoginContext.Provider value={json}>
        {children}
    </LoginContext.Provider>
    )
}

export {LoginProvider, LoginContext}
