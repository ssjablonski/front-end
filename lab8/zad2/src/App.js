import React from 'react';
import './App.css';
import {LoginProvider } from './context/LoginContext';
import LogIn from './components/LogIn';
import Register from './components/Register';

function App() {
  return (
    <LoginProvider>
      <LogIn />
      <Register />
    </LoginProvider>
  );
}

export default App;
