import React from 'react'
import {ColorContext} from '../context/ColorContext'

function Paragraph() {
  const color = React.useContext(ColorContext);
  return (
    <p style={{ color: color }}>Paragraph</p>
    
  )
}


export default Paragraph