import React from 'react'
import {ColorContext} from '../context/ColorContext'

function PrimaryHeadline() {
  const color = React.useContext(ColorContext);
  return (
    <h1 style={{ color: color }}>Headline</h1>
  )
}

export default PrimaryHeadline