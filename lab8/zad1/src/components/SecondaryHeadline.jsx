import React from 'react'
import {ColorContext} from '../context/ColorContext'

function SecondaryHeadline() {
  const color = React.useContext(ColorContext);
  return (
    <h2 style={{ color: color }}>SecondaryHeadline</h2>
  )
}

export default SecondaryHeadline