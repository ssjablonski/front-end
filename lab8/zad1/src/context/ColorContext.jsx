import React, { createContext} from 'react';

const ColorContext = createContext();

function ColorProvider({ children }) {
  const color = 'green'

  return (
    <ColorContext.Provider value={color}>
      {children}
    </ColorContext.Provider>
  );
};


export { ColorProvider, ColorContext };
