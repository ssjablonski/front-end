import React, { useState, useContext } from 'react';
import PrimaryHeadline from './components/PrimaryHeadline';
import Paragraph from './components/Paragraph';
import SecondaryHeadline from './components/SecondaryHeadline';
import { ColorProvider, ColorContext } from './context/ColorContext';


function App() {
  return (
    <ColorProvider>
      <PrimaryHeadline />
      <SecondaryHeadline />
      <Paragraph />
    </ColorProvider>
  )
}

export default App