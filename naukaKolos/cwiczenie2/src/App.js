import './App.css';
import ProductsList from './components/ProductsList';
import { LogsProvider } from './context/LogsContext';

function App() {
  return (
    <LogsProvider>
      <ProductsList />
    </LogsProvider>
    
  );
}

export default App;
