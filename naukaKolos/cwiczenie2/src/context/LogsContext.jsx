import React, {createContext} from "react";

const LogsContext = createContext();

export function LogsProvider({children}) {
    const logs = [
        {
            login: "login1",
            password: "password1",
            name: "Szef",
        },
        {
            login: "login2",
            password: "password2",
            name: "Kierownik",
        },
        {
            login: "login3",
            password: "password3",
            name: "Pracownik",
        },
    ]

    return (
        <LogsContext.Provider value={logs}>
            {children}
        </LogsContext.Provider>
    )
}