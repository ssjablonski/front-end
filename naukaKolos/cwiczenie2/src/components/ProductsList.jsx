import React,{useState, useEffect, useRef} from 'react'
import list from '../products.json'
import useDostepnosc from '../hooks/useDostepnosc'
import ProductsDetails from './ProductsDetails'
import AddProductForm from './AddProductForm'
import '../productsList.css'





function ProductsList() {
    const [products, setProducts] = useState([])
    const [render, setRender] = useState(false)
    const [filtred, setFiltred] = useState(false)
    const initRef = useRef({})
    const dostepne = useDostepnosc(products)

    useEffect(() => {
        setProducts(list.products)
    }, [])

    function handleClick(e) {
        switch (e.target.name) {
            case "details":
                const selectedProduct = products.find(product => product.model === e.target.id)
                initRef.current = selectedProduct
                setRender(!render)
                break;
            case "filtr":
                setFiltred(!filtred)
                break;
            default:
                break;
        }
    }

    const display = filtred ? dostepne : products

    return (
        <div>
            <h1>Eletronika</h1>
            <button name='filtr' onClick={handleClick}>Filtruj po Dostępności</button>
            {display.map(product => (
                <div key={product.nazwa} id='product'>
                    <ProductsDetails product={product} />
                    <button name='details' id={product.model} onClick={handleClick}>More info</button>
                    { initRef && initRef.current.model === product.model ? <p id='details'>{product.opis}</p> : null}
                </div>
            ))}
            <AddProductForm add={setProducts}/>
        </div>
    )
}

export default ProductsList