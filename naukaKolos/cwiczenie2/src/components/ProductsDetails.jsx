import React from 'react'

function ProductsDetails({product}) {
    return (
        <>
            <h2>{product.nazwa}</h2>
            <p>{product.marka}</p>
            <p>{product.model}</p>
            <p>{product.cena}</p>
            { product.dostepnosc ? <p>Dostępny</p> : <p>Niedostępny</p>}
        </>
    )
}

export default ProductsDetails