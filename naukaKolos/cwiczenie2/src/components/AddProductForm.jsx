import React from 'react'
import {useFormik} from 'formik'
import * as Yup from 'yup'
// import '../productsList.css'
function AddProductForm({add}) {

    function handleReset() {
        formik.resetForm()
    }

    const formik = useFormik({
        initialValues: {
            nazwa: '',
            marka: '',
            model: '',
            cena: '',
            dostepnosc: '',
            opis: ''
        },
        validationSchema: Yup.object({
            nazwa: Yup.string()
                .min(3, 'Nazwa musi mieć min. 3 znaki')
                .max(15, 'Nazwa może mieć max. 15 znaków')
                .required('Nazwa jest wymagana'),
            marka: Yup.string()
                .min(3, 'Marka musi mieć min. 3 znaki')
                .max(15, 'Marka może mieć max. 15 znaków')
                .required('Marka jest wymagana'),
            model: Yup.string()
                .min(3, 'Model musi mieć min. 3 znaki')
                .max(15, 'Model może mieć max. 15 znaków')
                .required('Model jest wymagany'),
            cena: Yup.number()
                .min(1, 'Cena musi być większa od 0')
                .required('Cena jest wymagana'),
            dostepnosc: Yup.boolean(),
            opis: Yup.string()
                .min(3, 'Opis musi mieć min. 3 znaki')
                .max(15, 'Opis może mieć max. 15 znaków')
                .required('Opis jest wymagany'),
        }),
        onSubmit: values => {
            console.log(values)
            add(prev => [...prev, values])
            formik.resetForm()

        }
    })

    return (
        <form onSubmit={formik.handleSubmit}>
            <label htmlFor="nazwa">Nazwa</label>
            <input type="text" name="nazwa" id="nazwa" onChange={formik.handleChange} value={formik.values.nazwa} />
            {formik.errors.nazwa ? <div>{formik.errors.nazwa}</div> : null}

            <label htmlFor="marka">Marka</label>
            <input type="text" name="marka" id="marka" onChange={formik.handleChange} value={formik.values.marka} />
            {formik.errors.marka ? <div>{formik.errors.marka}</div> : null}

            <label htmlFor="model">Model</label>
            <input type="text" name="model" id="model" onChange={formik.handleChange} value={formik.values.model} />
            {formik.errors.model ? <div>{formik.errors.model}</div> : null}

            <label htmlFor="cena">Cena</label>
            <input type="number" name="cena" id="cena" onChange={formik.handleChange} value={formik.values.cena} />
            {formik.errors.cena ? <div>{formik.errors.cena}</div> : null}

            <label htmlFor="dostepnosc">Dostępność</label>
            <input type="checkbox" name="dostepnosc" id="dostepnosc" onChange={formik.handleChange} value={formik.values.dostepnosc} />
            {formik.errors.dostepnosc ? <div>{formik.errors.dostepnosc}</div> : null}

            <label htmlFor="opis">Opis</label>
            <input type="text" name="opis" id="opis" onChange={formik.handleChange} value={formik.values.opis} />
            {formik.errors.opis ? <div>{formik.errors.opis}</div> : null}

            <button type="submit">Dodaj produkt</button>
            <button onClick={handleReset}>Resetuj formularz</button>
        </form>
    )
}

export default AddProductForm