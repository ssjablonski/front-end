import {useState, useEffect} from 'react'

function useDostepnosc(products) {
    const [dostepne, setDostepne] = useState([])

    useEffect(() => {
        const filteredProducts = products.filter(product => product.dostepnosc === true);
        setDostepne(filteredProducts);
    }, [products]);

    return dostepne;
}

export default useDostepnosc