import React from 'react'
import JediForm from './JediForm'

function isJedi(jedi, action) {
    return jedi.firstName === action.payload.firstName && jedi.lastName === action.payload.lastName
}

function reducer(state, action) {
    switch (action.type) {
        case 'GET_CURRENT_JEDI':
            return action.payload
        case 'SET_JOB':
            return state.map((jedi) => {
                if (isJedi(jedi, action)) {
                    return {
                        ...jedi,
                        job: action.payload.job
                    }
                } return jedi
            })
        case 'FINISH_TRAINING':
            return state.map((jedi) => {
                if (isJedi(jedi, action)) {
                    return {
                        ...jedi,
                        job: "Jedi Knight",
                        weapons: {
                            lightsaber: "blue"
                        }
                    }
                } return jedi
            })
            
        case 'JOIN_DARK_SIDE':
            return state.map((jedi) => {
                if (isJedi(jedi, action)) {
                    return {
                        ...jedi,
                        side: "dark",
                        job: "Sith",
                        weapons: {
                            lightsaber: "red"
                        }
                    }
                } 
                return jedi
            })
        case 'ADD_JEDI':
            return [...state, action.payload]
        case 'KILL_JEDI':
            return state.filter((jedi, index) => {
                return index !== action.payload.index
            })
        default:
            return state
    }
}
function Jedi() {
    const lukeState = {
        firstName : "Luke",
        lastName : "Skywalker",
        job : "moisture farmer",
        side : "light",
        weapons: {
            blaster : "DL-42"
        }
    };

    const [state, dispatch] = React.useReducer(reducer, [lukeState])


    return (
    <div>
        <JediForm dispatch={dispatch}/>
        <ul>
        {state.map((jedi, index) => {
            return (
                <li key={index}>
                    <p>Name: {jedi.firstName} {jedi.lastName}</p>
                    <p>Job: {jedi.job}</p>
                    <p>Side: {jedi.side}</p>
                    { (jedi.job === "Sith" || jedi.job === "Jedi Knight") ? <p>Lightsaber: {jedi.weapons.lightsaber}</p> : <p>Weapon: {jedi.weapons.blaster}</p>}
                    <input type="text" placeholder='SET JOB' id={`job-${index}`}></input>
                    <button onClick={() => dispatch({ type: 'SET_JOB', payload: {firstName: jedi.firstName, lastName: jedi.lastName, job: document.getElementById(`job-${index}`).value }})}>SET JOB</button>
                    <button onClick={() => dispatch({ type: 'FINISH_TRAINING', payload: {firstName: jedi.firstName, lastName: jedi.lastName}})}>FINISH TRAINING</button>
                    <button onClick={() => dispatch({ type: 'JOIN_DARK_SIDE', payload: {firstName: jedi.firstName, lastName: jedi.lastName}})}>JOIN DARK SIDE</button>
                    <button onClick={() => dispatch({ type: 'KILL_JEDI', payload: {index: index}})}>KILL JEDI</button>
                </li>
            )
        }
        )}
        </ul>
    </div>
    )
}

export default Jedi