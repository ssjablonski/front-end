import React, {useReducer, useState} from 'react'

function reducer(state, action) {
        switch (action.type) {
            case "increment":
                return { count: state.count + 1 };
            case "decrement":
                return { count: state.count - 1 };
            case "add":
                return { count: state.count + action.amount };
            case "subtract":
                return { count: state.count - action.amount };
            case "set":
                return { count: action.amount };   
            default:
                return { count: state.count }         
    } 
    }

function Cals() {

    const [state, dispatch] = useReducer(reducer, { count: 0 });
    const [amount, setAmount] = useState(0);
    
    return (
    <div>
        <p>{state.count}</p>
        <input type="number" onChange={e => setAmount(Number(e.target.value))}/>
        <button onClick={() => dispatch({ type: 'increment', amount: amount })}>INCREMENT</button>
        <button onClick={() => dispatch({ type: 'decrement', amount: amount})}>DECREMENT</button>
        <button onClick={() => dispatch({ type: 'add', amount: amount})}>ADD</button>
        <button onClick={() => dispatch({ type: 'subtract', amount: amount})}>SUBSTRACT</button>
        <button onClick={() => dispatch({ type: 'set', amount: amount})}>SET</button>
    </div>
    )
}

export default Cals