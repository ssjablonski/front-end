import React, {createContext, useReducer} from "react";

const ReducerContext = createContext();
export const useJediContext = () => React.useContext(ReducerContext);

export default function ReducerProvider({ children }) {

    function reducer(state, action) {
        function isJedi(jedi, action) {
            return jedi.firstName === action.payload.firstName && jedi.lastName === action.payload.lastName
        }   
        switch (action.type) {
            case 'GET_CURRENT_JEDI':
                return action.payload
            case 'SET_JOB':
                return state.map((jedi) => {
                    if (isJedi(jedi, action)) {
                        return {
                            ...jedi,
                            job: action.payload.job
                        }
                    }
                    return jedi
                })
            case 'FINISH_TRAINING':
                return state.map((jedi) => {
                    if (isJedi(jedi, action)) {
                        return {
                            ...jedi,
                            job: "Jedi Knight",
                            weapons: {
                                lightsaber: "blue"
                            }
                        }
                    } return jedi
                })
                
            case 'JOIN_DARK_SIDE':
                return state.map((jedi) => {
                    if (isJedi(jedi, action)) {
                        return {
                            ...jedi,
                            side: "dark",
                            job: "Sith",
                            weapons: {
                                lightsaber: "red"
                            }
                        }
                    } 
                    return jedi
                })
            case 'ADD_JEDI':
                return [...state, action.payload]
            case 'KILL_JEDI':
                return state.filter((jedi, index) => {
                    return index !== action.payload.index
                })
            default:
                return state
        }
    }

    const lukeState = {
        firstName : "Luke",
        lastName : "Skywalker",
        job : "moisture farmer",
        side : "light",
        weapons: {
            blaster : "DL-42"
        }
    };

    const [state, dispatch] = useReducer(reducer, [lukeState]);

    

    return (
        <ReducerContext.Provider value={{state, dispatch}}>
            {children}
        </ReducerContext.Provider>
    )
}
