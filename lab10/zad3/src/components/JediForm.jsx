import React, {useContext} from 'react'
import {useFormik} from 'formik'
import * as Yup from 'yup' 
import { useJediContext } from '../context/ReducerContext';

function JediForm() {
    const context = useJediContext()

    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            side: '',
            job: '',
            weapons: ''
        },
        validationSchema:  Yup.object({
            firstName: Yup.string().required("Required"),
            lastName: Yup.string().required("Required"),
            side: Yup.string().oneOf(['Dark', 'Light'], 'Choose "Dark" or "Light".').required('Required'),
            job: Yup.string().required('Required'),
            weapons: Yup.string().matches(/^[a-zA-Z]+-\d+$/, 'Invalid blaster format').required("Required")
        }),
        onSubmit: values => {
            console.log(values)
            context.dispatch({
                type: 'ADD_JEDI',
                payload: {...values, weapons: {blaster: values.weapons}}
            })
            alert("Jedi added")

        }    
    })
    
    return (
    <form onSubmit={formik.handleSubmit}>
        <input type="text" id='firstName' name='firstName' onChange={formik.handleChange} onBlur={formik.handleBlur} placeholder='First Name' value={formik.values.firstName}></input>
        {formik.touched.firstName && formik.errors.firstName ? <div>{formik.errors.firstName}</div> : null}
        <input type="text" id='lastName' name='lastName' onChange={formik.handleChange} onBlur={formik.handleBlur} placeholder='Last Name' value={formik.values.lastName}></input>
        {formik.touched.lastName && formik.errors.lastName ? <div>{formik.errors.lastName}</div> : null}
        <input type="text" id='side' name='side' onChange={formik.handleChange} onBlur={formik.handleBlur} placeholder='Side' value={formik.values.side}></input>
        {formik.touched.side && formik.errors.side ? <div>{formik.errors.side}</div> : null}
        <input type="text" id='job' name='job' onChange={formik.handleChange} onBlur={formik.handleBlur} placeholder='Job' value={formik.values.job}></input>
        {formik.touched.job && formik.errors.job ? <div>{formik.errors.job}</div> : null}
        <input type="text" id='weapons' name='weapons' onChange={formik.handleChange} onBlur={formik.handleBlur} placeholder='Weapon' value={formik.values.weapons.blaster}></input>
        {formik.touched.weapons && formik.errors.weapons ? <div>{formik.errors.weapons}</div> : null}
        <button type='submit'>Add Jedi</button>
    </form>
)
}

export default JediForm