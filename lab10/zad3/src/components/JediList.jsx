import React, {useReducer, useContext} from 'react'
import JediForm from './JediForm'
import { useJediContext } from '../context/ReducerContext';

function Jedi() {

    const context = useJediContext()

    return (
    <div>
        <JediForm />
        <ul>
        {context.state.map((jedi, index) => {
            return (
                <li key={index}>
                    <p>Name: {jedi.firstName} {jedi.lastName}</p>
                    <p>Job: {jedi.job}</p>
                    <p>Side: {jedi.side}</p>
                    { (jedi.job === "Sith" || jedi.job === "Jedi Knight") ? <p>Lightsaber: {jedi.weapons.lightsaber}</p> : <p>Weapon: {jedi.weapons.blaster}</p>}
                    <input type="text" placeholder='SET JOB' id={`job-${index}`}></input>
                    <button onClick={() => context.dispatch({ type: 'SET_JOB', payload: {firstName: jedi.firstName, lastName: jedi.lastName, job: document.getElementById(`job-${index}`).value }})}>SET JOB</button>
                    <button onClick={() => context.dispatch({ type: 'FINISH_TRAINING', payload: {firstName: jedi.firstName, lastName: jedi.lastName}})}>FINISH TRAINING</button>
                    <button onClick={() => context.dispatch({ type: 'JOIN_DARK_SIDE', payload: {firstName: jedi.firstName, lastName: jedi.lastName}})}>JOIN DARK SIDE</button>
                    <button onClick={() => context.dispatch({ type: 'KILL_JEDI', payload: {index: index}})}>KILL JEDI</button>
                </li>
            )
        }
        )}
        </ul>
    </div>
    )
}

export default Jedi