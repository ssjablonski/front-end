import './App.css';
import JediList from './components/JediList';
import ReducerProvider from './context/ReducerContext';
function App() {
  return (
    <ReducerProvider>
      <JediList />
      {/* <p>hej</p> */}
    </ReducerProvider>
  );
}

export default App;
